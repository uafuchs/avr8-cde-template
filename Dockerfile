FROM ubuntu:latest

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
	&& apt-get install apt-utils -qq \
	&& apt-get upgrade -qq \
	&& apt-get install python3 python3-apt tzdata locales locales-all unzip -qq \
  	&& rm -rf /var/lib/apt/lists/*
