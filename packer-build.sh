#!/usr/bin/bash

# see: https://levelup.gitconnected.com/how-to-combine-c-cmake-googletest-circleci-docker-and-why-e02d76c060a3
#      https://awesomeopensource.com/project/nardeas/ssh-agent
#      https://medium.com/trabe/use-your-local-ssh-keys-inside-a-docker-container-ea1d117515dc
#      https://www.ansible.com/blog/six-ways-ansible-makes-docker-compose-better

docker image rm -f ufuchs/avr8-cde:2021.1

packer build avr8-cde-docker.json

# [ "$?" -ne "0" ] && {
# 	echo failed
# 	exit
# }

# ssh-copy-id \
# 	-i ~/.ssh/id_ed25519 \
# 	-o PreferredAuthentications=password \
# 	-o PubkeyAuthentication=no \
# 	-p2222 \
# 	avr8@localhost

