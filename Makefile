
VERSION=2021.1

.PHONY: clean
clean:
	docker image rm -f ufuchs/avr8-cde:${VERSION}

.PHONY: avr8-cde
avr8-cde: 
	packer build  -var 'version=${VERSION}' avr8-cde-docker.json		
