#!/bin/bash

ssh-copy-id \
	-i ~/.ssh/id_ed25519 \
	-o PreferredAuthentications=password \
	-o PubkeyAuthentication=no \
	-p2222 \
	avr8@localhost
